//
//  PMPBaseViewController.m
//  PimpMyPic
//
//  Created by Alexey Titov on 13/08/14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPBaseViewController.h"
#import "PMPSideMenuViewController.h"
#import "PMPUser.h"
#import "PMPImagePicker.h"

@interface PMPBaseViewController ()

@end

@implementation PMPBaseViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view setBackgroundColor:MAIN_BAR_TINT_COLOR];
    
    if (self.navigationController) {
        UIBarButtonItem *menu = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemBookmarks target:self action:@selector(showSideMenu)];
        self.navigationItem.leftBarButtonItem = menu;
    }
}

- (void)showSideMenu
{
    PMPSideMenuViewController *sideMenu = (PMPSideMenuViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [sideMenu presentLeftMenuViewController];
}


@end
