//
//  PMPProfileViewController.m
//  PimpMyPic
//
//  Created by Alexey Titov on 13/08/14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPProfileViewController.h"
#import "PMPSideMenuViewController.h"
#import "PMPUser.h"
#import "PMPImagePicker.h"
#import "PMPActiveAccountService.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "MBProgressHUD+TextHUD.h"

@interface PMPProfileViewController ()

@property (weak, nonatomic) IBOutlet PFImageView *userAvatar;
@property (weak, nonatomic) IBOutlet UITextField *userLastName;
@property (weak, nonatomic) IBOutlet UITextField *userFirstName;
@property (nonatomic, strong) PMPImagePicker *imagePicker;
@property (nonatomic, strong) PMPUser *user;

@end

@implementation PMPProfileViewController

- (IBAction)changeAvatarButtonDidPressed:(id)sender
{
    __weak PMPProfileViewController *weakSelf = self;
    self.imagePicker = [[PMPImagePicker alloc]init];
    [self.imagePicker showInView:self.view
                       withTitle:@"PickImage"
                      completion:^(UIImage *resultImage) {
                          weakSelf.userAvatar.image = resultImage;
                      }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self fillUserInfo];
}

- (IBAction)saveProfileButtonDidPressed:(id)sender
{
    [MBProgressHUD showHUDAddedTo:self.view withText:@"Saving user info..." animated:YES];
    NSData *userAvatarData = [NSData dataWithData:UIImagePNGRepresentation(self.userAvatar.image)];
    self.user.firstName = self.userFirstName.text;
    self.user.lastName = self.userLastName.text;

    PFFile *avatarUser = [PFFile fileWithName:@"Image.png" data:userAvatarData];
    __weak PMPProfileViewController *weakSelf = self;
    [avatarUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        weakSelf.user.image = avatarUser;
        [weakSelf.user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
                [PMPActiveAccountService updateActiveUser:weakSelf.user];
            }
        }];
    }];
}

- (void)showSideMenu
{
    PMPSideMenuViewController *sideMenu = (PMPSideMenuViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [sideMenu presentLeftMenuViewController];
}

- (void)fillUserInfo
{
    [MBProgressHUD showHUDAddedTo:self.view withText:@"Fetching user info..." animated:YES];
    
    __weak PMPProfileViewController *weakSelf = self;
    [PMPActiveAccountService getSignedInUserWithCompletion:^(PMPUser *user) {
        weakSelf.user = user;
        weakSelf.userFirstName.text = weakSelf.user.firstName;
        weakSelf.userLastName.text = weakSelf.user.lastName;
        
        if (weakSelf.user.image) {
            [weakSelf.userAvatar setFile:weakSelf.user.image];
            [weakSelf.userAvatar loadInBackground];
        }
        else {
            NSURL *url = [NSURL URLWithString:weakSelf.user.avatarURLString];
            [weakSelf.userAvatar sd_setImageWithURL:url];
        }
        [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
    }];
}

@end
