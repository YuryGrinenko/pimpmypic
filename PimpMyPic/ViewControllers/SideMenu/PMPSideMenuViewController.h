//
//  PMPSideMenuViewController.h
//  PimpMyPic
//
//  Created by Alexey Titov on 13/08/14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "RESideMenu.h"

@interface PMPSideMenuViewController : RESideMenu

- (void)logoutUser;

@end
