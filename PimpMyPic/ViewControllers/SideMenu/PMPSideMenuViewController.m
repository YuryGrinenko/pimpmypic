//
//  PMPSideMenuViewController.m
//  PimpMyPic
//
//  Created by Alexey Titov on 13/08/14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPSideMenuViewController.h"
#import "PMPActiveAccountService.h"

#import "PMPIntroViewController.h"

@interface PMPSideMenuViewController ()

@end

@implementation PMPSideMenuViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuViewController"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
#warning CHANGE TO 1 TO DISPLAY INTRO
    if (1) {
        [self showIntro];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self showLoginIfNeededAnimated:NO];
}

- (void)showIntro {
    self.view.alpha = 0.0;
    
    PMPIntroViewController *introController = [PMPIntroViewController new];
    __weak PMPSideMenuViewController *weakSelf = self;
    [introController setDismissBlock:^{
        weakSelf.view.alpha = 1.0;
    }];
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [window addSubview:introController.view];
    [self addChildViewController:introController];
    [introController didMoveToParentViewController:self];
}

- (void)logoutUser
{
    [PMPActiveAccountService deleteSignedInUser];
    [self hideMenuViewController];
    [self showLoginIfNeededAnimated:YES];
}

- (void)showLoginIfNeededAnimated:(BOOL)animated
{
    if ([PMPActiveAccountService isUserLogined] == NO) {
        UIViewController *loginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
        [self presentViewController:loginViewController animated:animated completion:nil];
    }
}

@end
