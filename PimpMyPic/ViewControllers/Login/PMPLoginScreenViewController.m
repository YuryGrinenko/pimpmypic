//
//  PMPLoginScreenViewController.m
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 11.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPLoginScreenViewController.h"
#import "PMPSocialAccountService.h"
#import "PMPUser+Methods.h"
#import "PMPActiveAccountService.h"
#import "MBProgressHUD+TextHUD.h"

@interface PMPLoginScreenViewController ()

@property (nonatomic, strong) PMPUser *user;
@property (nonatomic, strong) PMPSocialAccountService *socialAccountService;

@end

@implementation PMPLoginScreenViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _socialAccountService = [[PMPSocialAccountService alloc] init];
}

- (IBAction)loginButtonPressed:(UIButton *)sender
{
    void(^loginErrorBlock)(NSString *) = ^(NSString *errorMessage){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:errorMessage
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles: nil];
        [alert show];
    };
    
    [MBProgressHUD showHUDAddedTo:self.view withText:@"Login..." animated:YES];
    
    __weak PMPLoginScreenViewController *weakSelf = self;
    [self.socialAccountService loadUserInfoFromFaceBookWithCompletion:^(NSDictionary * userInfo, NSString * errorMessage) {
       dispatch_async(dispatch_get_main_queue(), ^{
           if (userInfo != nil) {
               weakSelf.user = [PMPUser userAccountInfoFromDictionary:userInfo];
               [PMPActiveAccountService setNewSignedInUser:weakSelf.user completion:^(NSError *error) {
                   [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
                   if (error == nil) {
                      [self dismissViewControllerAnimated:YES completion:nil];
                   }
                   else {
                       loginErrorBlock(@"Something goes wrong. Please try again");
                   }
               }];
           }
           else {
               loginErrorBlock(errorMessage);
               [MBProgressHUD hideAllHUDsForView:weakSelf.view animated:YES];
           }
       });
    }];
}

@end
