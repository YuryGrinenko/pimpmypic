//
//  PMPFeedViewController.h
//  PimpMyPic
//

//  Created by Роман Щербаков on 19.08.14.

//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMPBaseViewController.h"

@class PMPPost;

typedef void(^GetPostCompletionBlock)(PMPPost *post);

@interface PMPFeedViewController : PMPBaseViewController

@end
