//
//  PMPFeedViewController.m
//  PimpMyPic
//
//  Created by Роман Щербаков on 19.08.14.

//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPFeedViewController.h"
#import "PMPImagePicker.h"
#import "PMPPostTableViewCell.h"
#import "PMPPostProvider.h"
#import "PMPPost.h"
#import "PMPHeaderSectionView.h"
#import "PMPEditorViewController.h"
#import "PMPEffectView.h"
#import "PMPActiveAccountService.h"
#import "PMPUser.h"
#import "PMPPostCreatedDateFormatter.h"
#import "MBProgressHUD+TextHUD.h"

@interface PMPFeedViewController () <RSObserver>

@property (strong, nonatomic) IBOutlet UITableView *feedTableView;

@property (nonatomic, strong) PMPImagePicker *imagePicker;
@property (strong, nonatomic) PMPPostProvider *postProvider;
@property (strong, nonatomic) PMPPostCreatedDateFormatter *postsCreatedDateFormatter;
    
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loaderActivityIndicator;
@end

@implementation PMPFeedViewController

- (IBAction)pickImageButtonDidPressed:(id)sender
{
    __weak PMPFeedViewController *weakSelf = self;
    self.imagePicker = [[PMPImagePicker alloc]init];
    [self.imagePicker showInView:self.view
                           withTitle:@"PickImage"
                           completion:^(UIImage *resultImage) {
                               GetPostCompletionBlock completionBlock = ^(PMPPost *post) {
                                   [weakSelf.postProvider addPost:post];
                                   [weakSelf.feedTableView insertSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
                               };
                               UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
                               PMPEditorViewController *editor = [storyboard instantiateViewControllerWithIdentifier:@"editorViewController"];
                               editor.completionBlock = completionBlock;
                               [editor setReceivedImageFromCameraRoll:resultImage];
                               [weakSelf.navigationController pushViewController:editor animated:YES];
                          }];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _postsCreatedDateFormatter = [PMPPostCreatedDateFormatter new];
        _postProvider = [[PMPPostProvider alloc] init];
        [_postProvider registerObserver:self];
    }
    return self;
}

- (void)dealloc
{
    [_postProvider removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.feedTableView setBackgroundColor:MAIN_BAR_TINT_COLOR];
    [MBProgressHUD showHUDAddedTo:self.view withText:@"Loading Feed..." animated:YES];
    
    UINib *nib = [UINib nibWithNibName:@"PMPHeaderSectionView" bundle:[NSBundle mainBundle]];
    [self.feedTableView registerNib:nib forHeaderFooterViewReuseIdentifier:@"headerViewIdentifier"];
    [self fetchNextPage];
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}

- (void)fetchNextPage
{
    self.loaderActivityIndicator.hidden = NO;
    [self.loaderActivityIndicator startAnimating];
    [self.postProvider fetchNextPage];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.postProvider count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMPPostTableViewCell *postCell = [tableView dequeueReusableCellWithIdentifier:@"postCellIdentifier" forIndexPath:indexPath];
    PMPPost *currentPost = [self.postProvider postForIndex:indexPath.section];
    [postCell setupWithPost:currentPost createdDateFormatter:self.postsCreatedDateFormatter];
    if (indexPath.section == self.postProvider.count - 1) {
        [self fetchNextPage];
    }
    return postCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 36.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    PMPHeaderSectionView *headerSectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"headerViewIdentifier"];
    PMPPost *currentPost = [self.postProvider postForIndex:section];
    [headerSectionView fillHeaderSectionWithUser:currentPost.user];
    return headerSectionView;
}

#pragma mark - RSObserver

- (void)contentDidChanged:(PMPPostProvider *)postProvider skip:(NSUInteger)skip
{
    [self.loaderActivityIndicator stopAnimating];
    self.loaderActivityIndicator.hidden = YES;
    [self.feedTableView beginUpdates];
    NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(skip, [postProvider count] - skip)];
    [self.feedTableView insertSections:indexes withRowAnimation:UITableViewRowAnimationFade];
    [self.feedTableView endUpdates];
}

@end
