//
//  PMPIntroViewController.h
//  PimpMyPic
//
//  Created by Yury Grinenko on 29.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DismissBlock)();

@interface PMPIntroViewController : UIViewController

- (void)setDismissBlock:(DismissBlock)dismissBlock;

@end
