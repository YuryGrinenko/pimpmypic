//
//  PMPIntroViewController.m
//  PimpMyPic
//
//  Created by Yury Grinenko on 29.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPIntroViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "UIImage+GIF.h"

static const CGPoint kPimpWordDestinationCenter = {107.0, 175.0};
static const CGPoint kMyWordDestinationCenter = {108.0, 212.0};
static const CGPoint kPicWordDestinationCenter = {164.0, 230};

@interface PMPIntroViewController () <AVAudioPlayerDelegate>

@property (nonatomic, weak) IBOutlet UIImageView *pimpWordImageView;
@property (nonatomic, weak) IBOutlet UIImageView *myWordImageView;
@property (nonatomic, weak) IBOutlet UIImageView *picWordImageView;


@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *fireworksImageViews;

@property (nonatomic, weak) IBOutlet UIImageView *faceImageView;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic, copy) DismissBlock dismissBlock;

@end

@implementation PMPIntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    self.faceImageView.alpha = 0.0f;
    self.view.frame = [[[[UIApplication sharedApplication] delegate] window] bounds];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.view.superview bringSubviewToFront:self.view];
    [self startAudioPlayer];
    [self startAnimations];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)startAudioPlayer {
    NSError *error = nil;
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"pimp_my_ride" withExtension:@"mp3"];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    [self.audioPlayer setDelegate:self];
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer play];
}

- (void)startAnimations {
    __weak PMPIntroViewController *weakSelf = self;

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf startPimpWordAnimation];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf startMyWordAnimation];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf startPicWordAnimation];
    });
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(7.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakSelf startFaceAnimation];
    });
}

- (void)startPimpWordAnimation {
    __weak PMPIntroViewController *weakSelf = self;
    [UIView animateWithDuration:5.0 animations:^{
        [weakSelf.pimpWordImageView setCenter:kPimpWordDestinationCenter];
    }];
}

- (void)startMyWordAnimation {
    __weak PMPIntroViewController *weakSelf = self;
    [UIView animateWithDuration:5.0 animations:^{
        [weakSelf.myWordImageView setCenter:kMyWordDestinationCenter];
    }];
}

- (void)startPicWordAnimation {
    __weak PMPIntroViewController *weakSelf = self;
    [UIView animateWithDuration:5.0 animations:^{
        [weakSelf.picWordImageView setCenter:kPicWordDestinationCenter];
    }];
}

- (void)startFaceAnimation {
    __weak PMPIntroViewController *weakSelf = self;
    [UIView animateWithDuration:5.0 animations:^{
        [weakSelf.faceImageView setAlpha:1.0];
    } completion:^(BOOL finished) {
        [weakSelf startFireworksAnimations];
    }];
}

- (void)startFireworksAnimations {
    for (UIImageView *imageView in self.fireworksImageViews) {
        [imageView setImage:[UIImage sd_animatedGIFNamed:@"fireworks"]];
    }
}

- (IBAction)dismiss {
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    if (self.dismissBlock) {
        self.dismissBlock();
    }

    if (self.parentViewController) {
        __weak PMPIntroViewController *weakSelf = self;
        [UIView animateWithDuration:1.0 animations:^{
            weakSelf.view.alpha = 0.0;
        } completion:^(BOOL finished) {
            [weakSelf.view removeFromSuperview];
            [weakSelf removeFromParentViewController];
        }];
    }
    else if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - AVAudioPlayerDelegate methods
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [self dismiss];
}

@end
