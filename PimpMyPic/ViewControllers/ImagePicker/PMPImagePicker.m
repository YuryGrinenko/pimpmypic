//
//  PMPImagePicker.m
//  PimpMyPic
//
//  Created by Admin on 15.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPImagePicker.h"

@interface PMPImagePicker () <UIActionSheetDelegate>

@property (nonatomic, copy) PMPImagePickerCompletionHandler completion;
@property (nonatomic, strong) UIImage *chosenImage;

@end

@implementation PMPImagePicker

- (void)showInView:(UIView *)view withTitle:(NSString *)title completion:(PMPImagePickerCompletionHandler)completion
{
    self.completion = completion;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        [self takeFromCameraRoll];
    } else {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title
                                                                 delegate:self
                                                        cancelButtonTitle:@"Cancel"
                                                   destructiveButtonTitle:nil otherButtonTitles:@"Take a Photo", @"Open From Camera Roll", nil];
      [actionSheet showInView:view];
   }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
       [self takePhoto];
    } else if (buttonIndex == 1) {
       [self takeFromCameraRoll];
    }
}

- (void)takePhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:picker animated:YES completion:NULL];
}
    
- (void)takeFromCameraRoll
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    if (self.completion) {
        self.completion(chosenImage);
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

@end
