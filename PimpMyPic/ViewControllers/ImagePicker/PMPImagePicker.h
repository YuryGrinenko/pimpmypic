//
//  PMPImagePicker.h
//  PimpMyPic
//
//  Created by Admin on 15.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^PMPImagePickerCompletionHandler) (UIImage *);

@interface PMPImagePicker : NSObject <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

- (void)showInView:(UIView *)view withTitle:(NSString *)title completion:(PMPImagePickerCompletionHandler)completion;

@end
