//
//  PMPLeftMenuViewController.m
//  PimpMyPic
//
//  Created by Alexey Titov on 13/08/14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPLeftMenuViewController.h"
#import "PMPSideMenuViewController.h"

@implementation PMPLeftMenuViewController

#pragma mark - IBActions

- (IBAction)feedSelected:(id)sender
{
    PMPSideMenuViewController *sideMenu = (PMPSideMenuViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    sideMenu.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentViewController"];
    [sideMenu hideMenuViewController];
}

- (IBAction)profileSelected:(id)sender
{
    PMPSideMenuViewController *sideMenu = (PMPSideMenuViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    sideMenu.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"profileViewController"];
    [sideMenu hideMenuViewController];
}

- (IBAction)aboutSelected:(id)sender
{
    PMPSideMenuViewController *sideMenu = (PMPSideMenuViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    sideMenu.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutViewController"];
    [sideMenu hideMenuViewController];
}

- (IBAction)logoutButtonDidPressed:(id)sender
{
    PMPSideMenuViewController *sideMenu = (PMPSideMenuViewController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [sideMenu logoutUser];
}

@end
