//
//  PMPEditorViewController.h
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 22.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMPEditEffectsActionProtocol.h"
#import "PMPEditEffectsProtocol.h"
#import "PMPFeedViewController.h"

@interface PMPEditorViewController : UIViewController

@property (nonatomic, strong) UIImage *receivedImageFromCameraRoll;
@property (nonatomic, copy) GetPostCompletionBlock completionBlock;

@end
