	//
//  PMPEditorViewController.m
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 22.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPEditorViewController.h"
#import "PMPEffectsEditorView.h"
#import "PMPEffectView.h"
#import "PMPEffectSelectionView.h"
#import "PMPEffectCategoriesToolbarView.h"
#import "PMPActiveAccountService.h"
#import "MBProgressHUD+TextHUD.h"
#import "PMPPost.h"

@interface PMPEditorViewController ()

@property (weak, nonatomic) IBOutlet UIView *effectsEditorViewContainerView;
@property (weak, nonatomic) IBOutlet UIView *effectsCategoryToolbarView;
@property (strong, nonatomic) PMPEffectsEditorView *effectsEditorView;
@property (strong, nonatomic) IBOutlet UIToolbar *bottomActionsToolbar;
@property (strong, nonatomic) IBOutlet UIImageView *effectCategoryToolbarBorderImage;

@property (strong, nonatomic) UIImage *finalImage;

@end

@implementation PMPEditorViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:MAIN_BAR_TINT_COLOR];
    [self.bottomActionsToolbar setBarTintColor:MAIN_BAR_TINT_COLOR];
    [self.bottomActionsToolbar setTintColor:MAIN_TINT_COLOR];
    [self.effectCategoryToolbarBorderImage setBackgroundColor:MAIN_TINT_COLOR];

    CGFloat borderWidth = 3.0f;
    self.effectsEditorViewContainerView.frame = CGRectInset(self.effectsEditorViewContainerView.frame, -borderWidth, -borderWidth);
    self.effectsEditorViewContainerView.layer.borderColor = MAIN_TINT_COLOR.CGColor;
    self.effectsEditorViewContainerView.layer.borderWidth = borderWidth;

    self.effectsEditorView = [[PMPEffectsEditorView alloc] initWithImage:self.receivedImageFromCameraRoll];
    [self.effectsEditorViewContainerView addSubview:self.effectsEditorView];
    PMPEffectCategoriesToolbarView *effectCategoriesToolbarView = [[PMPEffectCategoriesToolbarView alloc] initWithDelegate:self.effectsEditorView];
    [self.effectsCategoryToolbarView addSubview:effectCategoriesToolbarView];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (IBAction)backToFeedButtonDidPressed:(id)sender
{
    [self dismiss];
}

- (IBAction)saveToCameraRollEditedImageButtonDidPressed:(id)sender
{
    if (!self.finalImage) {
        [self.effectsEditorView prepareForScreenshot];
        self.finalImage = [self imageFromEditorView];
    }
    UIImageWriteToSavedPhotosAlbum(self.finalImage, nil, nil, nil);
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [hud setMode:MBProgressHUDModeText];
    [hud setDetailsLabelText:@"Photo was saved to Camera Roll"];
    [hud hide:YES afterDelay:2.0];
}


- (IBAction)removeAllEffectsButtonDidPressed:(id)sender {
    [self.effectsEditorView deleteAllEffects];
}

- (IBAction)saveUserPostToParse:(id)sender
{
    [MBProgressHUD showHUDAddedTo:self.view withText:@"Please, wait..." animated:YES];
    PMPPost *post = [PMPPost new];
    if (!self.finalImage) {
        [self.effectsEditorView prepareForScreenshot];
        self.finalImage = [self imageFromEditorView];
    }
    NSData *imgData = UIImagePNGRepresentation(self.finalImage);
    post.image = [PFFile fileWithName:@"Image.png" data:imgData];
    __weak PMPPost *weakPost = post;
    __weak PMPEditorViewController *weakSelf = self;
    [PMPActiveAccountService getSignedInUserWithCompletion:^(PMPUser *user) {
        weakPost.user = user;
        [weakPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                if (!error) {
                    weakSelf.completionBlock(post);
                    [self dismiss];
                }
                else {
                    NSLog(@"%@", error);
                }
            });
        }];
    }];
    
}

- (UIImage *)imageFromEditorView
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(self.effectsEditorView.bounds.size, NO, [UIScreen mainScreen].scale);
    }
    else {
        
        UIGraphicsBeginImageContext(self.effectsEditorView.bounds.size);
    }
    
    [self.effectsEditorView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)dismiss {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
