//
//  PMPPostCreatedDateFormatter.m
//  PimpMyPic
//
//  Created by Yury Grinenko on 04.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPPostCreatedDateFormatter.h"

@interface PMPPostCreatedDateFormatter ()

@property (nonatomic, strong) NSDateFormatter *createdDateFormatter;

@end

@implementation PMPPostCreatedDateFormatter

- (id)init {
    self = [super init];
    if (self) {
        _createdDateFormatter = [NSDateFormatter new];
        [_createdDateFormatter setDateFormat:@"hh:mm, dd MMM y"];
    }
    return self;
}

- (NSString *)descriptionStringFromDate:(NSDate *)createdDate {
    return [self.createdDateFormatter stringFromDate:createdDate];
}

@end
