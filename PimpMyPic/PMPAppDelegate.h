//
//  PMPAppDelegate.h
//  PimpMyPic
//
//  Created by Yury Grinenko on 09.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
