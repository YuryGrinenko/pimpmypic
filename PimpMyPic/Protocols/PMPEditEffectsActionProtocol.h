//
//  PMPEditEffectsActionProtocol.h
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 27.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMPEffectView;

@protocol PMPEffectsEditingOptionsProviderProtocol <NSObject>

- (void)showEditingOptionsForEffect:(PMPEffectView *)effect;

@end
