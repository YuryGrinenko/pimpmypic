//
//  PMPEditEffectsProtocol.h
//  PimpMyPic
//
//  Created by Alexey Titov on 23/08/14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMPEffectView;

@protocol PMPEditEffectsProtocol <NSObject>

- (void)putUpEffect:(PMPEffectView *)effect;
- (void)putDownEffect:(PMPEffectView *)effect;
- (void)deleteEffect:(PMPEffectView *)effect;
- (void)deleteAllEffects;

@end
