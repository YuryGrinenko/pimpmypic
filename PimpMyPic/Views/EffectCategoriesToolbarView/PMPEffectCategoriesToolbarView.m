//
//  PMPEffectCategoriesToolbarView.m
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 03.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPEffectCategoriesToolbarView.h"
#import "PMPEffectCollectionViewCell.h"
#import "PMPCategoriesProvider.h"
#import "PMPEffectsCategory.h"
#import "PMPEffectSelectionView.h"

@interface PMPEffectCategoriesToolbarView()

@property (nonatomic, weak) IBOutlet UICollectionView *categoryCollectionView;
@property (strong, nonatomic) NSArray *categories;
@property (nonatomic, weak) id<PMPEffectDelegate> delegate;

@end

@implementation PMPEffectCategoriesToolbarView

- (id)initWithDelegate:(id<PMPEffectDelegate>)delegate
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0];
    if (self) {
        _categories = [PMPCategoriesProvider arrayOfCategories];
        _delegate = delegate;
        
        [self setBackgroundColor:MAIN_BAR_TINT_COLOR];
        [self.categoryCollectionView setBackgroundColor:MAIN_BAR_TINT_COLOR];
        [self.categoryCollectionView registerNib:[UINib nibWithNibName:@"PMPEffectCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"effectCellIdentifier"];
    }
    return self;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.categories count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMPEffectCollectionViewCell *categoryCell = [self.categoryCollectionView dequeueReusableCellWithReuseIdentifier:@"effectCellIdentifier" forIndexPath:indexPath];
    
    [categoryCell setBackgroundColor:MAIN_BAR_TINT_COLOR];
    categoryCell.layer.cornerRadius  = 12.0f;
    categoryCell.layer.masksToBounds = YES;
    categoryCell.layer.borderWidth = 1.0f;
    categoryCell.layer.borderColor = MAIN_TINT_COLOR.CGColor;
    
    PMPEffectsCategory *currentCategory = [self.categories objectAtIndex:indexPath.row];
    NSLog(@"categoryID %@", currentCategory.objectID);
    [categoryCell setEffectImage:[UIImage imageWithData:currentCategory.icon]];
    return categoryCell;
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMPEffectsCategory *currentCategory = [self.categories objectAtIndex:indexPath.row];
    PMPEffectSelectionView *effectSelectionView = [[PMPEffectSelectionView alloc] initWithEffectsCategoryId:[currentCategory.effectsCategoryId integerValue] delegate:self.delegate];
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [window addSubview:effectSelectionView];
}


@end
