//
//  PMPEffectCategoriesToolbarView.h
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 03.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "PMPEffectSelectionView.h"
@protocol PMPEffectDelegate;

@interface PMPEffectCategoriesToolbarView : UIView

- (id)initWithDelegate:(id<PMPEffectDelegate>)delegate;

@end
