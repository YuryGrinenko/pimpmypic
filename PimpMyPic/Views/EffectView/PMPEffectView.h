//
//  PMPImageEffectViewController.h
//  PimpMyPic
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "PMPEditEffectsActionProtocol.h"
#import "PMPEditorViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface PMPEffectView : UIView

- (id)initWithImage:(UIImage *)image editingOptionsProvider:(id<PMPEffectsEditingOptionsProviderProtocol>)editingOptionsProvider;
- (void)hideMarque;

@end
