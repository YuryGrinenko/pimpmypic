//
//  PMPImageEffectViewController.m
//  PimpMyPic
//
//  Created by Admin on 22.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPEffectView.h"

@interface PMPEffectView ()

@property (weak, nonatomic) IBOutlet UIView *canvas;
@property (nonatomic, weak) IBOutlet UIImageView *effectImageView;
@property (nonatomic, weak) id<PMPEffectsEditingOptionsProviderProtocol> editingOptionsProvider;
@property (nonatomic, assign) CGFloat scale;
@property (nonatomic, assign) CGFloat rotation;
@property (nonatomic, assign) CGFloat firstX;
@property (nonatomic, assign) CGFloat firstY;
@property (nonatomic, strong) CAShapeLayer *marque;

@end

@implementation PMPEffectView

- (id)init
{
    self = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil][0];
    return self;
}

- (id)initWithImage:(UIImage *)image editingOptionsProvider:(id<PMPEffectsEditingOptionsProviderProtocol>)editingOptionsProvider;
{
    self = [self init];
    if (self) {
        _effectImageView.image = image;
        _editingOptionsProvider = editingOptionsProvider;
        [self setFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        if (!_marque) {
            _marque = [CAShapeLayer layer];
            _marque.fillColor = [[UIColor clearColor] CGColor];
            _marque.strokeColor = [[UIColor grayColor] CGColor];
            _marque.lineWidth = 1.0f;
            _marque.lineJoin = kCALineJoinRound;
            _marque.lineDashPattern = [NSArray arrayWithObjects:[NSNumber numberWithInt:10],[NSNumber numberWithInt:5], nil];
            _marque.bounds = CGRectMake(self.effectImageView.frame.origin.x, self.effectImageView.frame.origin.y, 0, 0);
            _marque.position = CGPointMake(self.effectImageView.frame.origin.x + _canvas.frame.origin.x, self.effectImageView.frame.origin.y + _canvas.frame.origin.y);
        }
        [[self layer] addSublayer:_marque];
    }
    return self;
}

- (IBAction)handlePanAction:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.superview];
    if(recognizer.state == UIGestureRecognizerStateBegan) {
        self.firstX = self.center.x;
        self.firstY = self.center.y;
    }
    translation = CGPointMake(self.firstX + translation.x, self.firstY + translation.y);
    self.center = translation;
    [self showOverlayWithFrame:self.effectImageView.frame];
}

- (IBAction)handlePinchAction:(UIPinchGestureRecognizer *)recognizer
{
    if(recognizer.state == UIGestureRecognizerStateBegan) {
        self.scale = 1.0;
    }
    CGFloat deltaScale = 1.0 - (self.scale - recognizer.scale);
    self.transform = CGAffineTransformScale(self.transform, deltaScale, deltaScale);
    self.scale = recognizer.scale;
    [self showOverlayWithFrame:self.effectImageView.frame];
}

- (IBAction)handleRotateAction:(UIRotationGestureRecognizer *)recognizer
{
    if(recognizer.state == UIGestureRecognizerStateEnded) {
        self.rotation = 0.0;
        return;
    }
    CGFloat deltaRotation = 0.0 - (self.rotation - recognizer.rotation);
    self.transform = CGAffineTransformRotate(self.transform, deltaRotation);
    self.rotation = recognizer.rotation;
    [self showOverlayWithFrame:self.effectImageView.frame];
}

- (IBAction)handleTapAction:(id)sender
{
    _marque.hidden = YES;
}

- (IBAction)handleLongPressAction:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateBegan && self.editingOptionsProvider && [self.editingOptionsProvider respondsToSelector:@selector(showEditingOptionsForEffect:)]) {
        [self.editingOptionsProvider showEditingOptionsForEffect:self];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return ![gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && ![gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]];
}

-(void)showOverlayWithFrame:(CGRect)frame
{
    if (![_marque actionForKey:@"linePhase"]) {
        CABasicAnimation *dashAnimation;
        dashAnimation = [CABasicAnimation animationWithKeyPath:@"lineDashPhase"];
        [dashAnimation setFromValue:[NSNumber numberWithFloat:0.0f]];
        [dashAnimation setToValue:[NSNumber numberWithFloat:15.0f]];
        [dashAnimation setDuration:0.5f];
        [dashAnimation setRepeatCount:HUGE_VALF];
        [_marque addAnimation:dashAnimation forKey:@"linePhase"];
    }
    
    _marque.bounds = CGRectMake(frame.origin.x, frame.origin.y, 0, 0);
    _marque.position = CGPointMake(frame.origin.x + _canvas.frame.origin.x, frame.origin.y + _canvas.frame.origin.y);
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, frame);
    [_marque setPath:path];
    CGPathRelease(path);
    
    _marque.hidden = NO;
}

- (void)hideMarque {
    [_marque setHidden:YES];
}

@end
