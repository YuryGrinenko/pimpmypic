//
//  PMPLeftMenuButton.m
//  PimpMyPic
//
//  Created by Yury Grinenko on 02.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPLeftMenuButton.h"

@implementation PMPLeftMenuButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    [self setTitleColor:MAIN_TEXT_COLOR forState:UIControlStateNormal];
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.layer.shadowOpacity = 1.0f;
    self.layer.shadowRadius = 1.0f;
    
    UIFont *font = [UIFont fontWithName:@"PT Banana Split" size:40.0];
    [self.titleLabel setFont:font];
}

@end
