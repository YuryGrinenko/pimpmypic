//
//  PMPEffectSelectionView.m
//  PimpMyPic
//
//  Created by Роман Щербаков on 24.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPEffectSelectionView.h"
#import "PMPEffectsProvider.h"
#import "PMPEffect.h"
#import "PMPEffectCollectionViewCell.h"

@interface PMPEffectSelectionView ()

@property (weak, nonatomic) IBOutlet UICollectionView *effectCollectionView;
@property (strong, nonatomic) NSArray *effects;
@property (nonatomic, weak) id<PMPEffectDelegate> delegate;

@end

@implementation PMPEffectSelectionView

- (id)initWithEffectsCategoryId:(NSUInteger)effectsCategoryId delegate:(id<PMPEffectDelegate>)delegate
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"PMPEffectSelectionView" owner:self options:nil] objectAtIndex:0];
    if (self) {
        _effects = [NSArray arrayWithArray:[PMPEffectsProvider effectsByCategoryId:effectsCategoryId]];
        _delegate = delegate;
        [self.effectCollectionView registerNib:[UINib nibWithNibName:@"PMPEffectCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"effectCellIdentifier"];
        self.effectCollectionView.layer.cornerRadius  = 10.0f;
        self.effectCollectionView.layer.masksToBounds = YES;
    }
    return self;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.effects count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PMPEffectCollectionViewCell *effectCell = [self.effectCollectionView dequeueReusableCellWithReuseIdentifier:@"effectCellIdentifier" forIndexPath:indexPath];
    PMPEffect *currentEffect = [self.effects objectAtIndex:indexPath.row];
    [effectCell setEffectImage:[UIImage imageWithData:currentEffect.icon]];
    return effectCell;
}

#pragma mark - Private Methods

- (void)dismissView
{
   [self removeFromSuperview];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(effectSelectionView:didSelectEffectImage:)]) {
        PMPEffect *currentEffect = [self.effects objectAtIndex:indexPath.row];
        [self.delegate effectSelectionView:self didSelectEffectImage:currentEffect];
    }
    [self dismissView];
}

- (IBAction)cancelPressed:(id)sender
{
    [self dismissView];
}

@end
