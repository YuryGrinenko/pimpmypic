//
//  PMPEffectSelectionView.h
//  PimpMyPic
//
//  Created by Роман Щербаков on 24.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PMPEffectDelegate;

@class PMPEffect;

@interface PMPEffectSelectionView : UIView

- (id)initWithEffectsCategoryId:(NSUInteger)effectsCategoryId delegate:(id<PMPEffectDelegate>)delegate;

@end

@protocol PMPEffectDelegate <NSObject>

- (void)effectSelectionView:(PMPEffectSelectionView *)effectView didSelectEffectImage:(PMPEffect *)effect;

@end
