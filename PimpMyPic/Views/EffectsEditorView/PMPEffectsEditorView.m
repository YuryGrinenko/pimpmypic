//
//  PMPEffectsEditorView.m
//  PimpMyPic
//
//  Created by Alexey Titov on 22/08/14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPEffectsEditorView.h"
#import "PMPEffectView.h"
#import "PMPEffectSelectionView.h"
#import "PMPEffect.h"

@interface PMPEffectsEditorView () <UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UIView *effectsContainerView;

@property (strong, nonatomic) PMPEffectView *selectedEffectView;

@end

@implementation PMPEffectsEditorView

- (id)init
{
    self = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil][0];
    [self setClipsToBounds:YES];
    return self;
}

- (id)initWithImage:(UIImage *)image
{
    self = [self init];
    if (self) {
        self.imageView.image = image;
    }
    return self;
}

- (void)addEffectView:(PMPEffectView *)effectView
{
    [self.effectsContainerView addSubview:effectView];
}

- (NSInteger)indexOfEffect:(PMPEffectView *)effect
{
    return [self.effectsContainerView.subviews indexOfObject:effect];
}

- (NSInteger)numberOfEffects
{
    return [self.effectsContainerView.subviews count];
}

#pragma mark - PMPEffectDelegate

- (void)effectSelectionView:(PMPEffectSelectionView *)effectSelectionView didSelectEffectImage:(PMPEffect *)effect
{
    UIImage *effectImage = [UIImage imageWithData:effect.icon];
    PMPEffectView *effectView = [[PMPEffectView alloc] initWithImage:effectImage editingOptionsProvider:self];
    [self addEffectView:effectView];
}

#pragma mark - EditEffectsProtocol

- (void)putUpEffect:(PMPEffectView *)effect
{
    NSInteger index = [self indexOfEffect:effect];
    if (index < [self numberOfEffects] - 1) {
        [self.effectsContainerView exchangeSubviewAtIndex:index withSubviewAtIndex:index + 1];
    }
}

- (void)putDownEffect:(PMPEffectView *)effect
{
    NSInteger index = [self indexOfEffect:effect];
    if (index > 0) {
        [self.effectsContainerView exchangeSubviewAtIndex:index withSubviewAtIndex:index - 1];
    }
}

- (void)deleteEffect:(PMPEffectView *)effect
{
    [effect removeFromSuperview];
}

- (void)deleteAllEffects
{
    for (PMPEffectView *effect in self.effectsContainerView.subviews) {
        [effect removeFromSuperview];
    }
}

- (void)prepareForScreenshot {
    for (PMPEffectView *effect in self.effectsContainerView.subviews) {
        [effect hideMarque];
    }
}

#pragma mark - PMPEffectsEditingOptionsProviderProtocol methods

- (void)showEditingOptionsForEffect:(PMPEffectView *)effect
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Edit Effect"
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:@"Delete effect" otherButtonTitles:@"Put effect up", @"Put effect down ", nil];
    self.selectedEffectView = effect;
    [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0)
    {
        //delete
        [self deleteEffect:self.selectedEffectView];
        NSLog(@"Selected effect deleted");
        
    }
    else if (buttonIndex == 1)
    {
        //PutUp
        [self putUpEffect:self.selectedEffectView];
        NSLog(@"Selected effect puted up");
    }
    else if (buttonIndex == 2)
    {
        //PutDown
        [self putDownEffect:self.selectedEffectView];
        NSLog(@"Selected effect puted down");
    }
}

@end
