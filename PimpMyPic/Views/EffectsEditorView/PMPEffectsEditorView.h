//
//  PMPEffectsEditorView.h
//  PimpMyPic
//
//  Created by Alexey Titov on 22/08/14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMPEditEffectsProtocol.h"
#import "PMPEffectSelectionView.h"

@protocol PMPEffectsEditingOptionsProviderProtocol;

@interface PMPEffectsEditorView : UIView <PMPEditEffectsProtocol, PMPEffectDelegate, PMPEffectsEditingOptionsProviderProtocol>

- (id)initWithImage:(UIImage *)image;
- (void)prepareForScreenshot;

@end
