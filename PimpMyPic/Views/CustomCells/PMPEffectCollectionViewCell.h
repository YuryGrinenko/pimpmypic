//
//  PMPEffectCollectionViewCell.h
//  PimpMyPic
//
//  Created by Роман Щербаков on 25.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMPEffectCollectionViewCell : UICollectionViewCell

- (void)setEffectImage:(UIImage *)effectImage;

@end
