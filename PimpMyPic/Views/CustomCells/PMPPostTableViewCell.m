//
//  PMPPostTableViewCell.m
//  PimpMyPic
//
//  Created by Роман Щербаков on 18.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPPostTableViewCell.h"
#import "PMPPost.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "PMPPostCreatedDateFormatter.h"
#import "PMPUser.h"
#import "PMPActiveAccountService.h"

@interface PMPPostTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *countLikesLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateCreatedLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomDividerView;

@property (weak, nonatomic) IBOutlet PFImageView *postImageView;
@property (weak, nonatomic) IBOutlet UIButton *likesButton;

@property (strong, nonatomic) PMPPost *post;
@property (assign, nonatomic) BOOL isLiked;
@property (assign, nonatomic) int likesCount;

@end

@implementation PMPPostTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.contentView setBackgroundColor:MAIN_BAR_TINT_COLOR];
    [self.bottomDividerView setBackgroundColor:MAIN_TINT_COLOR];
    [self.likesButton setTintColor:MAIN_TINT_COLOR];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    
    [self.postImageView.file cancel];
    [self.postImageView setImage:nil];
}

- (void)setupWithPost:(PMPPost *)post createdDateFormatter:(PMPPostCreatedDateFormatter *)createdDateFormatter {
    self.dateCreatedLabel.text = [createdDateFormatter descriptionStringFromDate:post.createdAt];
    self.postImageView.file = post.image;
    [self.postImageView loadInBackground];
    
    NSString *activeUserId = [PMPActiveAccountService signedInUserId];
    
    self.isLiked = NO;
    self.likesCount = [post.likes count];
    
    for (NSString *userId in post.likes) {
        if ([userId isEqualToString:activeUserId]) {
            self.isLiked = YES;
            break;
        }
    }
    
    if (self.isLiked) {
        [self.likesButton setImage:[UIImage imageNamed:@"to_unlike_icon.png"] forState:UIControlStateNormal];
    } else {
        [self.likesButton setImage:[UIImage imageNamed:@"to_like_icon.png"] forState:UIControlStateNormal];
    }
    
    self.countLikesLabel.text = [NSString stringWithFormat:@"%i", self.likesCount];
    self.post = post;
}

- (IBAction)likesButtonPressed:(id)sender
{
    NSString *activeUserId = [PMPActiveAccountService signedInUserId];
    
    if (self.isLiked) {
        [self.post removeObject:activeUserId forKey:@"likes"];
        [self.post saveInBackground];
        self.likesCount--;
        self.countLikesLabel.text = [NSString stringWithFormat:@"%i", self.likesCount];
        [self.likesButton setImage:[UIImage imageNamed:@"to_like_icon.png"] forState:UIControlStateNormal];
        self.isLiked = NO;
    } else {
        [self.post addObject:activeUserId forKey:@"likes"];
        [self.post saveInBackground];
        self.likesCount++;
        self.countLikesLabel.text = [NSString stringWithFormat:@"%i", self.likesCount];
        [self.likesButton setImage:[UIImage imageNamed:@"to_unlike_icon.png"] forState:UIControlStateNormal];
        self.isLiked = YES;
    }
}

@end
