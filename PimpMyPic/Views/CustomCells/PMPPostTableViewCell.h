//
//  PMPPostTableViewCell.h
//  PimpMyPic
//
//  Created by Роман Щербаков on 18.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMPPost, PMPPostCreatedDateFormatter;

@interface PMPPostTableViewCell : UITableViewCell

- (void)setupWithPost:(PMPPost *)post createdDateFormatter:(PMPPostCreatedDateFormatter *)createdDateFormatter;

@end
