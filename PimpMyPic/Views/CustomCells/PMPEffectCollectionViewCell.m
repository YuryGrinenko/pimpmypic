//
//  PMPEffectCollectionViewCell.m
//  PimpMyPic
//
//  Created by Роман Щербаков on 25.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPEffectCollectionViewCell.h"

@interface PMPEffectCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *effectImageView;

@end

@implementation PMPEffectCollectionViewCell

- (void)setEffectImage:(UIImage *)effectImage
{
    self.effectImageView.image = effectImage;
}

@end
