//
//  PMPHeaderSectionView.h
//  PimpMyPic
//
//  Created by Роман Щербаков on 24.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PMPUser;

@interface PMPHeaderSectionView : UITableViewHeaderFooterView

- (void)fillHeaderSectionWithUser:(PMPUser *)user;

@end
