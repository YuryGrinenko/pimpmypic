//
//  PMPHeaderSectionView.m
//  PimpMyPic
//
//  Created by Роман Щербаков on 24.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPHeaderSectionView.h"
#import "PMPUser.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PMPHeaderSectionView ()

@property (weak, nonatomic) IBOutlet PFImageView *userAvatarView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIView *backgroundColorView;

@end

@implementation PMPHeaderSectionView

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self.backgroundColorView setBackgroundColor:MAIN_BAR_TINT_COLOR];
}

- (void)fillHeaderSectionWithUser:(PMPUser *)user
{
    self.userNameLabel.text = [NSString stringWithFormat:@"%@ %@", user.firstName, user.lastName];
    if (user.image) {
        self.userAvatarView.file = user.image;
        [self.userAvatarView loadInBackground];
    }
    else {
        [self.userAvatarView sd_setImageWithURL:[NSURL URLWithString:user.avatarURLString]];
    }
}

@end
