//
//  PMPEffectsProvider.m
//  PimpMyPic
//
//  Created by Роман Щербаков on 24.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPEffectsProvider.h"
#import "PMPEffectsCategory.h"
#import "PMPEffect.h"

@implementation PMPEffectsProvider

const CGFloat PMPImageСompressionQuality = 0.5f;

+ (NSArray *)effectsByCategoryId:(NSUInteger)categoryId
{
    PMPEffectsCategory *effectsCategory = [[PMPEffectsCategory MR_findByAttribute:@"effectsCategoryId" withValue:[NSNumber numberWithInteger:categoryId]] firstObject];
    NSArray *effects = [NSArray arrayWithArray:[effectsCategory.effects allObjects]];
    if (effects != nil) {
        return effects;
    }
    else {
        return nil;
    }
}

@end
