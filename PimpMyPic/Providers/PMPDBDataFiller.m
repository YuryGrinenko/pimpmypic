//
//  PMPDBDataFiller.m
//  PimpMyPic
//
//  Created by Yury Grinenko on 04.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPDBDataFiller.h"

#import "PMPEffectsCategory.h"
#import "PMPEffect.h"
#import "CoreData+MagicalRecord.h"

@implementation PMPDBDataFiller

- (void)fillDB {
    [self resetDB];
    
    [MagicalRecord setupAutoMigratingCoreDataStack];
    
    NSArray *firstCategoryImagesNames = @[@"1", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"2", @"20", @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"3", @"30", @"31", @"32", @"33", @"34", @"35", @"36", @"37", @"38", @"39", @"4", @"40", @"41", @"42", @"43", @"44", @"45", @"46", @"47", @"48", @"49", @"5", @"50", @"6", @"7", @"8", @"9"];
    
    NSArray *secondCategoryImagesNames = @[@"201", @"202", @"203", @"204", @"205", @"206", @"207", @"208", @"209", @"210"];
    
    NSArray *thirdCategoryImagesNames = @[@"301", @"302", @"303", @"304", @"305", @"306", @"307"];
    
    NSArray *fourthCategoryImagesNames = @[@"401", @"402", @"403", @"404", @"405", @"406", @"407", @"408", @"409", @"410", @"411", @"412", @"413", @"414", @"415", @"416", @"417", @"418", @"419"];
    
    NSArray *fifthCategoryImagesNames = @[@"501", @"502", @"503", @"504", @"505", @"506", @"507", @"508", @"509", @"510", @"511", @"512", @"513"];
    
    NSArray *categoriesImagesArrays = @[firstCategoryImagesNames, secondCategoryImagesNames, thirdCategoryImagesNames, fourthCategoryImagesNames, fifthCategoryImagesNames];
    
    NSManagedObjectContext *context = [NSManagedObjectContext MR_defaultContext];
    
    for (int i = 0; i < [categoriesImagesArrays count]; i++) {
        NSArray *imagesNamesArray = categoriesImagesArrays[i];
        
        PMPEffectsCategory *category = [PMPEffectsCategory MR_createInContext:context];
        category.effectsCategoryId = @(i);
        category.icon = UIImagePNGRepresentation([UIImage imageNamed:imagesNamesArray[0]]);
        
        for (int j = 0; j < [imagesNamesArray count]; j++) {
            NSString *imageName = imagesNamesArray[j];
            UIImage *image = [UIImage imageNamed:imageName];
            if (image) {
                PMPEffect *effect = [PMPEffect MR_createInContext:context];
                effect.effectId = @([imageName integerValue]);
                effect.icon = UIImagePNGRepresentation([UIImage imageNamed:imageName]);
                [category addEffectsObject:effect];
            }
        }
    }
    
    [context MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        if (error) {
            NSLog(@"saving context error: %@", error);
        }
    }];
}

- (NSString *)dbStore {
    return [MagicalRecord defaultStoreName];
}

- (void)resetDB {
    NSString *dbStore = [self dbStore];
    
    NSError *error = nil;
    
    NSURL *storeURL = [NSPersistentStore MR_urlForStoreName:dbStore];
    
    if ([[NSFileManager defaultManager] removeItemAtURL:storeURL error:&error] == NO) {
        NSLog(@"An error has occurred while deleting %@", dbStore);
        NSLog(@"Error description: %@", error.description);
    }
}
@end
