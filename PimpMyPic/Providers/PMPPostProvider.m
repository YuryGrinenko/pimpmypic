//
//  PMPPostProvider.m
//  PimpMyPic
//
//  Created by Роман Щербаков on 18.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPPostProvider.h"
#import "PMPUser.h"

const NSUInteger PMPPostProviderDefaultPageSize = 8;

@interface PMPPostProvider ()

@property (nonatomic, strong) NSHashTable *observers;

@property (nonatomic, strong) NSMutableArray *posts;
@property (nonatomic, assign) NSUInteger pageSize;
@property (nonatomic, assign) NSUInteger skip;
@property (nonatomic, assign) NSUInteger currentPageIndex;

@property (nonatomic, assign, getter = isFetching) BOOL fetching;

@end

@implementation PMPPostProvider

- (id)init
{
    return [self initWithPageSize:PMPPostProviderDefaultPageSize];
}

- (id)initWithPageSize:(NSUInteger)pageSize
{
    NSParameterAssert(pageSize > 0);
    
    self = [super init];
    if (self) {
        _observers = [NSHashTable weakObjectsHashTable];
        
        _posts = [NSMutableArray array];
        _pageSize = pageSize;
        _skip = 0;
        _currentPageIndex = 0;
    }
    return self;
}

#pragma mark - Public Methods

- (void)addPost:(PMPPost *)post
{
    [self.posts insertObject:post atIndex:0];
}

- (PMPPost *)postForIndex:(NSUInteger)idx
{
    return [self.posts objectAtIndex:idx];
}

- (NSUInteger)count
{
    return self.posts.count;
}

- (void)fetchNextPage
{
    if ([self isFetching]) {
        return;
    }

    self.skip = [self count];
    
    PFQuery *postsQuery = [PFQuery queryWithClassName:@"PMPPost"];
    postsQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
    [postsQuery includeKey:@"user"];
    [postsQuery orderByDescending:@"createdAt"];
    [postsQuery setLimit:self.pageSize];
    [postsQuery setSkip:self.skip];

    [self setFetching:YES];

    __weak typeof(self) weakSelf = self;
    [postsQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [weakSelf setFetching:NO];
        if (!error) {
            weakSelf.currentPageIndex++;
            [weakSelf.posts addObjectsFromArray:objects];
            [weakSelf notifyObservers:weakSelf skip:weakSelf.skip];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

#pragma mark - RSSubject

- (void)registerObserver:(id<RSObserver>)observer
{
    [self.observers addObject:observer];
}

- (void)removeObserver:(id<RSObserver>)observer
{
    [self.observers removeObject:observer];
}

- (void)notifyObservers:(PMPPostProvider *)postProvider skip:(NSUInteger)skip
{
    for (id<RSObserver> observer in [self.observers copy]) {
        if ([observer respondsToSelector:@selector(contentDidChanged:skip:)]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [observer contentDidChanged:postProvider skip:skip];
            });
        }
    }
}

@end
