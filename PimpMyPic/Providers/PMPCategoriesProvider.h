//
//  PMPCategoriesProvider.h
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 03.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPCategoriesProvider : NSObject

+ (NSArray *)arrayOfCategories;

@end
