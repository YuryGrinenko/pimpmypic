//
//  PMPPostProvider+StubbedPosts.h
//  PimpMyPic
//
//  Created by Роман Щербаков on 19.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPPostProvider.h"

@interface PMPPostProvider (StubbedPosts)

- (NSMutableArray *)fillStubbedPosts;

@end
