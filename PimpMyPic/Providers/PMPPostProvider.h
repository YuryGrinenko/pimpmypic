//
//  PMPPostProvider.h
//  PimpMyPic
//
//  Created by Роман Щербаков on 18.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PMPPost;
@class PMPPostProvider;

extern const NSUInteger PMPPostProviderDefaultPageSize;

@protocol RSObserver <NSObject>

@optional

- (void)contentDidChanged:(PMPPostProvider *)postProvider skip:(NSUInteger)skip;

@end

@protocol RSSubject <NSObject>

- (void)registerObserver:(id <RSObserver>)observer;
- (void)removeObserver:(id <RSObserver>)observer;
- (void)notifyObservers:(PMPPostProvider *)postProvider skip:(NSUInteger)skip;

@end

@interface PMPPostProvider : NSObject <RSSubject>

- (id)initWithPageSize:(NSUInteger)pageSize;
- (void)fetchNextPage;
- (NSUInteger)count;
- (PMPPost *)postForIndex:(NSUInteger)idx;
- (void)addPost:(PMPPost *)post;

@end
