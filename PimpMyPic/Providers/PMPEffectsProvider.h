//
//  PMPEffectsProvider.h
//  PimpMyPic
//
//  Created by Роман Щербаков on 24.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPEffectsProvider : NSObject

+ (NSArray *)effectsByCategoryId:(NSUInteger)categoryId;

@end
