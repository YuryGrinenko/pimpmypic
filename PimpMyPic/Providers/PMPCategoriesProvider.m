//
//  PMPCategoriesProvider.m
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 03.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPCategoriesProvider.h"
#import "PMPEffectsCategory.h"

@implementation PMPCategoriesProvider

+ (NSArray *)arrayOfCategories
{
    NSArray *categories = [PMPEffectsCategory MR_findAll];
    if (categories != nil) {
        return categories;
    }
    else {
        return nil;
    }
}

@end
