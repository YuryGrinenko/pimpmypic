//
//  PMPDBDataFiller.h
//  PimpMyPic
//
//  Created by Yury Grinenko on 04.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPDBDataFiller : NSObject

- (void)fillDB;

@end
