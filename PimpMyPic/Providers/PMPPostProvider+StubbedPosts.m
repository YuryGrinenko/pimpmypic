//
//  PMPPostProvider+StubbedPosts.m
//  PimpMyPic
//
//  Created by Роман Щербаков on 19.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPPostProvider+StubbedPosts.h"
#import "PMPPost.h"

@implementation PMPPostProvider (StubbedPosts)

- (NSMutableArray *)fillStubbedPosts
{
    NSMutableArray *stubbedPosts = [[NSMutableArray alloc] init];
    PMPPost *post1 = [[PMPPost alloc] init];
    PMPPost *post2 = [[PMPPost alloc] init];
    PMPPost *post3 = [[PMPPost alloc] init];
    PMPPost *post4 = [[PMPPost alloc] init];
    
//    post1.createDate = @"08.08.2014";
//    post1.postId = 1;
    post1.imageURLString = @"http://t2.gstatic.com/images?q=tbn:ANd9GcTzY9-QRJ7KMqvQtEOkgpyPT1GgbAZg89-u91IRIWOxn851s9OXyQ";
//    post1.fakeUserName = @"Jim Carrey";
//    post1.likesCount = 10;
    [stubbedPosts addObject:post1];
    
//    post2.createDate = @"09.08.2014";
//    post2.postId = 2;
    post2.imageURLString = @"https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTjcVsZp6U0wN9Ivfkwu6lfOJQWTOkfT4OBWtNYLYAk2j8NQgsO";
//    post2.fakeUserName = @"Arnold Schwarzenegger";
//    post2.likesCount = 22;
    [stubbedPosts addObject:post2];
    
//    post3.createDate = @"10.08.2014";
//    post3.postId = 3;
    post3.imageURLString = @"https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTMKLyeRm-EMrYZEqlSRezStkmCxCWAShqI67PACPN_DYVtOphj";
//    post3.fakeUserName = @"Antonio Banderas";
//    post3.likesCount = 4;
    [stubbedPosts addObject:post3];
    
//    post4.createDate = @"11.08.2014";
//    post4.postId = 4;
    post4.imageURLString = @"https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcS_hEYIvlyfnHUGFP7YqUVKz4Jcrg8wUMr2CnQeCaXbx9Ke8THj9w";
//    post4.fakeUserName = @"John Travolta";
//    post4.likesCount = 17;
    [stubbedPosts addObject:post4];
    
    return stubbedPosts;
}

@end
