//
//  PMPAppDelegate.m
//  PimpMyPic
//
//  Created by Yury Grinenko on 09.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPAppDelegate.h"
#import "PMPEffectsProvider.h"

#import "PMPAppearanceConfigurator.h"
#import "MagicalRecord+DBInsert.h"
#import "PMPUser.h"
#import "PMPPost.h"
#import "PMPActivity.h"
#import <Parse/Parse.h>

#ifdef FILLDBTARGET

#import "PMPDBDataFiller.h"

#endif

@implementation PMPAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
#ifdef FILLDBTARGET
    PMPDBDataFiller *filler = [PMPDBDataFiller new];
    [filler fillDB];
#else
    [MagicalRecord setupAutoMigratingCoreDataStackWithDBInsertingIfNeeded];
    [self setupAppearance];
    [self registerParseSubclasses];
    [self startParseSessionWithLaunchOptions:launchOptions];
#endif
    
    return YES;
}

- (void)setupAppearance {
    PMPAppearanceConfigurator *configurator = [PMPAppearanceConfigurator new];
    [configurator configurateAppearance];
}

- (void)registerParseSubclasses {
    [PMPUser registerSubclass];
    [PMPPost registerSubclass];
    [PMPActivity registerSubclass];
}

- (void)startParseSessionWithLaunchOptions:(NSDictionary *)launchOptions {
    [Parse setApplicationId:@"zNJCu7dcBg9j121SeFlHiYS0hCdz4TKk9fdeDr0T"
                  clientKey:@"hij1qHA781TRbhbkhdVzcDoL5bdVVNFJDrrKcADm"];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
}

@end
