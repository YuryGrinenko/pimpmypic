//
//  PMPAppearanceConfigurator.h
//  PimpMyPic
//
//  Created by Yury Grinenko on 02.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PMPAppearanceConfigurator : NSObject

- (void)configurateAppearance;

@end
