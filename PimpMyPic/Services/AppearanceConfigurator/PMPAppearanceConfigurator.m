//
//  PMPAppearanceConfigurator.m
//  PimpMyPic
//
//  Created by Yury Grinenko on 02.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPAppearanceConfigurator.h"

@implementation PMPAppearanceConfigurator

- (void)configurateAppearance {
    [[UINavigationBar appearance] setBarTintColor:MAIN_BAR_TINT_COLOR];
    [[UINavigationBar appearance] setTintColor:MAIN_TINT_COLOR];
    
    UIFont *font = [UIFont fontWithName:@"PT Banana Split" size:30.0];
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
    shadow.shadowOffset = CGSizeMake(0, 1.5);
    shadow.shadowBlurRadius = 1.0f;
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                           NSForegroundColorAttributeName : MAIN_TEXT_COLOR,
                                                           NSShadowAttributeName : shadow,
                                                           NSFontAttributeName : font
                                                           }];
}

@end
