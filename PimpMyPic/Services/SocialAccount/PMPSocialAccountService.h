//
//  PMPSocialAccountService.h
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 11.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^PMPUserInfoCompletionHandler)(NSDictionary *, NSString*) ;

@interface PMPSocialAccountService : NSObject

- (void)loadUserInfoFromFaceBookWithCompletion:(PMPUserInfoCompletionHandler)completion;

@end
