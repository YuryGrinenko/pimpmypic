//
//  PMPSocialAccountService.m
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 11.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPSocialAccountService.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>

@interface PMPSocialAccountService()

@property (nonatomic, retain) ACAccountStore *accountStore;
@property (nonatomic, retain) ACAccount *facebookAccount;

@end

@implementation PMPSocialAccountService

- (void)loadUserInfoFromFaceBookWithCompletion:(PMPUserInfoCompletionHandler)completion
{    
    if(!_accountStore){
        _accountStore = [[ACAccountStore alloc] init];
    }
    
    ACAccountType *facebookTypeAccount = [_accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    __weak PMPSocialAccountService *weakSelf = self;
    [_accountStore requestAccessToAccountsWithType:facebookTypeAccount
                                           options:@{ACFacebookAppIdKey: FACEBOOK_APP_ID, ACFacebookPermissionsKey: @[@"email"]}
                                        completion:^(BOOL granted, NSError *error) {
                                            if(granted){
                                                NSArray *accounts = [weakSelf.accountStore accountsWithAccountType:facebookTypeAccount];
                                                weakSelf.facebookAccount = [accounts lastObject];
                                                NSLog(@"Success");
                                                [self loadUserInfoWithCompletion:^(NSDictionary * userInfo) {
                                                    NSLog(@"Dictionary contains: %@", userInfo );
                                                    if (userInfo == nil) {
                                                         completion(nil,@"No Internet connection...");
                                                    } else {
                                                         completion(userInfo,nil);
                                                    }
                                                }];
                                            }else{
                                                // Errors
                                                NSLog(@"Fail");
                                                NSLog(@"Error: %@", error);
                                                if([error code]== ACErrorAccountNotFound){
                                                    completion(nil, @"Account not found. Please setup your account in settings app.");
                                                }
                                                else{
                                                    completion(nil, @"Account access denied.");
                                                }
                                            }
                                        }];
    
    
}

- (void)loadUserInfoWithCompletion:(void (^)(NSDictionary *))completion
{
    __block NSDictionary *userInfoDictionary;
    
    NSURL *meurl = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    
    SLRequest *merequest = [SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodGET URL:meurl parameters:nil];
    
    merequest.account = _facebookAccount;
    
    [merequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (error) {
            userInfoDictionary = nil;
        } else {
            userInfoDictionary =[NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        }
        completion(userInfoDictionary);
    }];
}

@end
