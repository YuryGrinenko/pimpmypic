//
//  PMPActiveAccountService.h
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 15.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PMPUser;

typedef void(^SetUserCompletionBlock)(NSError *error);
typedef void(^GetUserCompletionBlock)(PMPUser *user);

@interface PMPActiveAccountService : NSObject

+ (void)setNewSignedInUser:(PMPUser *)user completion:(SetUserCompletionBlock)completionBlock;
+ (void)updateActiveUser:(PMPUser *)newUser;
+ (BOOL)isUserLogined;
+ (NSString *)signedInUserId;

+ (void)getSignedInUserWithCompletion:(GetUserCompletionBlock)completionBlock;

+ (void)deleteSignedInUser;

@end