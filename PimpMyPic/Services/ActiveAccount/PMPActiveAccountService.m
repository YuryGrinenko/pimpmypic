//
//  PMPActiveAccountService.m
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 15.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPActiveAccountService.h"
#import "PMPUser.h"

static NSString *const kUserIdKey = @"userId";

@implementation PMPActiveAccountService

static PMPUser *user = nil;

+ (void)setNewSignedInUser:(PMPUser *)user completion:(SetUserCompletionBlock)completionBlock
{
    void(^saveSuccessBlock)() = ^{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:user.userId forKey:kUserIdKey];
        [defaults synchronize];
    };
    
    PFQuery *query = [PFQuery queryWithClassName:@"PMPUser"];
    [query setCachePolicy:kPFCachePolicyNetworkElseCache];
    [query whereKey:@"userId" equalTo:user.userId];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            completionBlock(error);
        }
        else if ([objects count] > 0) {
            saveSuccessBlock();
            completionBlock(nil);
        }
        else {
            [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (error == nil) {
                    saveSuccessBlock();
                }
                completionBlock(error);
            }];
        }
    }];
}

+ (void)updateActiveUser:(PMPUser *)newUser
{
    user = newUser;
}

+ (void)getSignedInUserWithCompletion:(GetUserCompletionBlock)completionBlock {
    if ([self isUserLogined] == NO) {
        completionBlock(nil);
        return;
    }
    
    if (user) {
        completionBlock(user);
    }
    else {
        NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:kUserIdKey];
        
        PFQuery *query = [PFQuery queryWithClassName:@"PMPUser"];
        [query setCachePolicy:kPFCachePolicyNetworkElseCache];
        [query whereKey:@"userId" equalTo:userId];
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if ([objects count] > 0) {
                user = objects[0];
                completionBlock(user);
            }
            else {
                completionBlock(nil);
            }
        }];
    }
}

+ (BOOL)isUserLogined
{
    NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:kUserIdKey];
    return userId != nil;
}

+ (NSString *)signedInUserId {
    return [[NSUserDefaults standardUserDefaults] valueForKey:kUserIdKey];
}

+ (void)deleteSignedInUser
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:kUserIdKey];
    [defaults synchronize];
    
    user = nil;
}

@end
