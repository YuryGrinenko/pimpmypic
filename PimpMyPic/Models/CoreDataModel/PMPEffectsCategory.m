//
//  PMPEffectsCategory.m
//  PimpMyPic
//
//  Created by Yury Grinenko on 04.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPEffectsCategory.h"
#import "PMPEffect.h"


@implementation PMPEffectsCategory

@dynamic effectsCategoryId;
@dynamic icon;
@dynamic effects;

@end
