//
//  PMPEffect.m
//  PimpMyPic
//
//  Created by Роман Щербаков on 24.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPEffect.h"


@implementation PMPEffect

@dynamic effectId;
@dynamic icon;
@dynamic effectsCategory;

@end
