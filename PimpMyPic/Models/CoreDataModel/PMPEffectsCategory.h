//
//  PMPEffectsCategory.h
//  PimpMyPic
//
//  Created by Yury Grinenko on 04.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PMPEffect;

@interface PMPEffectsCategory : NSManagedObject

@property (nonatomic, retain) NSNumber * effectsCategoryId;
@property (nonatomic, retain) id icon;
@property (nonatomic, retain) NSSet *effects;
@end

@interface PMPEffectsCategory (CoreDataGeneratedAccessors)

- (void)addEffectsObject:(PMPEffect *)value;
- (void)removeEffectsObject:(PMPEffect *)value;
- (void)addEffects:(NSSet *)values;
- (void)removeEffects:(NSSet *)values;

@end
