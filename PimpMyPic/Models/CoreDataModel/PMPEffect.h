//
//  PMPEffect.h
//  PimpMyPic
//
//  Created by Роман Щербаков on 24.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PMPEffect : NSManagedObject

@property (nonatomic, retain) NSNumber * effectId;
@property (nonatomic, retain) id icon;
@property (nonatomic, retain) NSManagedObject *effectsCategory;

@end
