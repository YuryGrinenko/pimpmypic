//
//  PMPActivity.m
//  PimpMyPic
//
//  Created by Yury Grinenko on 30.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPActivity.h"
#import <Parse/PFObject+Subclass.h>

@implementation PMPActivity

@dynamic type;
@dynamic post;
@dynamic user;

+ (NSString *)parseClassName {
    return @"PMPActivity";
}

@end
