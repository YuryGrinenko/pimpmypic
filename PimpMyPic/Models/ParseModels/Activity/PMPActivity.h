//
//  PMPActivity.h
//  PimpMyPic
//
//  Created by Yury Grinenko on 30.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Parse/Parse.h>
#import "PMPPost.h"
#import "PMPUser.h"

@interface PMPActivity : PFObject <PFSubclassing>

@property (nonatomic, strong) NSString *type;

@property (nonatomic, strong) PMPPost *post;
@property (nonatomic, strong) PMPUser *user;

@end
