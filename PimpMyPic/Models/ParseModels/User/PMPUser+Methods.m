//
//  PMPUser+Methods.m
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 13.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPUser+Methods.h"

@implementation PMPUser (Methods)

+ (PMPUser *)userAccountInfoFromDictionary:(NSDictionary *)dictionary
{
    PMPUser * user = [[PMPUser alloc]init];
    user.firstName = [dictionary objectForKey:@"first_name"];
    user.lastName = [dictionary objectForKey:@"last_name"];
    user.userId = [dictionary objectForKey:@"id"];
    user.avatarURLString = [NSString stringWithFormat: kUserFacebookProfileURLString, user.userId];
    return user;
}

@end
