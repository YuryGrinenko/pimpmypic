//
//  PMPUser.h
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 12.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

static NSString *const kUserFacebookProfileURLString = @"https://graph.facebook.com/%@/picture?type=large";

@interface PMPUser : PFObject <PFSubclassing>

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *avatarURLString;

@property (nonatomic, strong) PFFile *image;
@property (nonatomic, strong) PFRelation *posts;
@property (nonatomic, strong) PFRelation *activities;

@end
