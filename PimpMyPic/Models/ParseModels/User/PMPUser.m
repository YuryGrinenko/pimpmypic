//
//  PMPUser.m
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 12.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPUser.h"
#import <Parse/PFObject+Subclass.h>

@implementation PMPUser

@dynamic firstName;
@dynamic lastName;
@dynamic userId;
@dynamic avatarURLString;

@dynamic image;
@dynamic posts;
@dynamic activities;

+ (NSString *)parseClassName {
    return @"PMPUser";
}

@end
