//
//  PMPUser+Methods.h
//  PimpMyPic
//
//  Created by Artyom Gavryushov on 13.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPUser.h"

@interface PMPUser (Methods)

+ (PMPUser *)userAccountInfoFromDictionary:(NSDictionary *)dictionary;

@end
