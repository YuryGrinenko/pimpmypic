//
//  PMPPost.m
//  PimpMyPic
//
//  Created by Роман Щербаков on 18.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "PMPPost.h"
#import <Parse/PFObject+Subclass.h>

@implementation PMPPost

@dynamic createdAt;
@dynamic imageURLString;
@dynamic image;
@dynamic user;
@dynamic likes;

+ (void)initialize {
    [PMPPost registerSubclass];
}


+ (NSString *)parseClassName {
    return @"PMPPost";
}

@end
