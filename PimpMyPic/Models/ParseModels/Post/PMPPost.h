//
//  PMPPost.h
//  PimpMyPic
//
//  Created by Роман Щербаков on 18.08.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@class PMPUser;

@interface PMPPost : PFObject <PFSubclassing>

@property (nonatomic, strong) NSDate *createdAt;
@property (nonatomic, strong) NSString *imageURLString;
@property (nonatomic, strong) PFFile *image;

@property (nonatomic, strong) PMPUser *user;
@property (nonatomic, strong) NSArray *likes;

//@property (nonatomic, assign) BOOL isLiked;
//@property (nonatomic, assign) NSUInteger likesCount;

@end
