//
//  MBProgressHUD+TextHUD.m
//  PimpMyPic
//
//  Created by Yury Grinenko on 02.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "MBProgressHUD+TextHUD.h"

@implementation MBProgressHUD (TextHUD)

+ (MBProgressHUD *)showHUDAddedTo:(UIView *)view withText:(NSString *)text animated:(BOOL)animated {
    MBProgressHUD *hud = [[MBProgressHUD alloc] init];
    hud.detailsLabelText = text;
    [view addSubview:hud];
    [hud show:YES];
    
    return hud;
}

@end
