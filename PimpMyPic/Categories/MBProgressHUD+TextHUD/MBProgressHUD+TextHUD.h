//
//  MBProgressHUD+TextHUD.h
//  PimpMyPic
//
//  Created by Yury Grinenko on 02.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "MBProgressHUD.h"

@interface MBProgressHUD (TextHUD)

+ (MBProgressHUD *)showHUDAddedTo:(UIView *)view withText:(NSString *)text animated:(BOOL)animated;

@end
