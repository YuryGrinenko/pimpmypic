//
//  MagicalRecord+DBInsert.m
//  PimpMyPic
//
//  Created by Yury Grinenko on 04.09.14.
//  Copyright (c) 2014 YalantisCourses. All rights reserved.
//

#import "MagicalRecord+DBInsert.h"
#import "CoreData+MagicalRecord.h"

@implementation MagicalRecord (DBInsert)

+ (void)setupAutoMigratingCoreDataStackWithDBInsertingIfNeeded {
    NSURL *url = [NSPersistentStore MR_urlForStoreName:[self defaultStoreName]];
    NSString *path = [url path];

    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:path] == NO) {
        NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[self defaultStoreName]];
        [self createPathToStoreFileIfNeccessary:url];
        NSError *error = nil;
        if ([fileManager copyItemAtPath:defaultDBPath toPath:path error:&error] == NO) {
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        }
    }

    [self setupAutoMigratingCoreDataStack];
}

+ (void)createPathToStoreFileIfNeccessary:(NSURL *)urlForStore {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *pathToStore = [urlForStore URLByDeletingLastPathComponent];
    
    NSError *error = nil;
    BOOL pathWasCreated = [fileManager createDirectoryAtPath:[pathToStore path] withIntermediateDirectories:YES attributes:nil error:&error];
    
    if (!pathWasCreated) {
        [MagicalRecord handleErrors:error];
    }
}

@end
